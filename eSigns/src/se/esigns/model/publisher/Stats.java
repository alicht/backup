package se.esigns.model.publisher;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import se.esigns.model.sign.Sign;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Stats")
public class Stats {
    public int displayTime;
    public int charged;
    public HashMap<Sign, Tuple> publishedInSigns=
	    new HashMap<Sign, Tuple>();

    public Stats() {
    }
}