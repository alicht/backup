package se.esigns.model.commercial;

import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.Printer;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;

public class MediaCommercial extends Commercial {
	
    private Image image;

    public MediaCommercial(PublishingPolicy policy, String id,
	    Publisher publisher,Set<Sign> SignsSet, String imageFile) {
	super(policy, id, publisher,SignsSet);
	ImageLoader loader = new ImageLoader();
	
	if (imageFile == null) {
//	    FileDialog fileChooser = new FileDialog(shell, SWT.OPEN);
//	    imageFile = fileChooser.open();
	}
	ImageData[] imageData = loader.load(imageFile);
	if (imageData.length <= 0)
	    throw new RuntimeException();
//	this.image = new Image(shell.getDisplay(), imageData[0]);
    }




    @Override
    public void simulateCommercial(int x, int y) {
	// code that affects the GUI
//	Display display = shell.getDisplay();
	System.out.println(image.getImageData().scanlinePad);
	image.getImageData().scanlinePad = 40;
	System.out.println(image.getImageData().scanlinePad);
//	shell.addPaintListener(new PaintListener() {
//	    public void paintControl(PaintEvent event) {
//		try {
//		    GC gc = new GC(display);
//		    event.gc.drawImage(image,x,y);
//		    // Clean up
//		    gc.dispose();
//		}
//		catch (Exception e) {
//		    MessageBox messageBox = new MessageBox(shell,
//			    SWT.ICON_ERROR);
//		    messageBox.setMessage("Error printing test image");
//		    messageBox.open();
//		}
//	    }
//	});
    }
}
