package se.esigns.view.publisher;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Display;

import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.publisher.IPublisherDatabase;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;

public class PublisherView  implements IPublisherDatabaseListener {
    private PublisherController publisherController;
//    private IPublisherDatabase publisherData;
    private List<IPublisherDatabaseListener> dataListeners=new ArrayList<>();
  
    private PublisherLogin login;
    public PublisherView(Display display){
//	this.publisherData=data;
	
	login = new PublisherLogin(display,this);
	dataListeners.add(login);
    }
    
    public void initUI(){
	login.initUI();
    }
    
    public void registerController(PublisherController controller){
	publisherController=controller;
	
    }
    public PublisherController getController(){
	return publisherController;
    }
    
    
    
   
    public void onLogin() {
	// TODO Auto-generated method stub
	login.onLogin();
    }
    public void onLoginFail(String reason){
	login.onLoginFailed(reason);
    }

    public void onLogout() {

    }
   
    public void onRegister() {
	// TODO Auto-generated method stub
	login.onRegister();
    }
    
    public void onRegisterFail(String reason){
	login.onRegisterFailed(reason);
    }

  
    public void onCommercials(List<Commercial> commercials) {
	// TODO Auto-generated method stub
	
    }

   public void onCreateCommercial(){
       
   }
    public void onEditCommercial(Commercial commercial) {
	// TODO Auto-generated method stub
	
    }
    
    public void onSigns(List<Sign> signs){
	
    }
    
    

    @Override
    public void onPublisherModelUpdate(Publisher publisher) {
	// TODO Auto-generated method stub
	
    }
    
  
    
}
