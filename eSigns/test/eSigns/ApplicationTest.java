package eSigns;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import technion.ac.il.softwareEngineering.esigns.SignsApplication;
import technion.ac.il.softwareEngineering.esigns.AppDatabase;
import technion.ac.il.softwareEngineering.esigns.controller.commercial.MediaCommercial;
import technion.ac.il.softwareEngineering.esigns.controller.commercial.TextCommercial;
import technion.ac.il.softwareEngineering.esigns.controller.management.Manager;
import technion.ac.il.softwareEngineering.esigns.controller.publisher.CommercialPublisher;
import technion.ac.il.softwareEngineering.esigns.controller.publisher.OfficialPublisher;
import technion.ac.il.softwareEngineering.esigns.controller.publisher.PublisherStatistics;
import technion.ac.il.softwareEngineering.esigns.controller.sign.AreaType;
import technion.ac.il.softwareEngineering.esigns.controller.sign.Sign;
import technion.ac.il.softwareEngineering.esigns.controller.sign.SignPricing;

public class ApplicationTest {
    AppDatabase db = new AppDatabase("dbxml");

    @Test
    public void test() throws JAXBException, IOException {
	SignsApplication app = new SignsApplication();
	// app.clear();
	app.start();
	AppDatabase db = new AppDatabase("dbxml");
	Sign s = new Sign("sign", new SignPricing(), "kiryat_bialik",
		AreaType.Urban, false);
	Sign s2 = new Sign("signTwo", new SignPricing(), "kfar_bialik",
		AreaType.Non_Urban, false);
	Display d = new Display();
	Shell shell;
	// db.addSign(s);
	// db.addSign(s2);
	// db.saveDatabase();
	// db.loadDatabase();

	Manager m = app.addManager("", "");
	OfficialPublisher o = m.addOfficial("officialPub", "4321");
	o.setDisplay(d);
	TextCommercial t = o.createTextPush("heyho", 5);
	TextCommercial t1 = o.createTextPush("PUSH TEXT NOTIFICATION", 5);
	t.simulateCommercial(0, 0);
	t1.simulateCommercial(0, 100);
	shell = o.getShell();

	shell.open();
	// shell.close();
	while (!shell.isDisposed()) {
	    if (!d.readAndDispatch()) {
		d.sleep();
		// t1.simulateCommercial(0, 400);
	    }
	}
	s.addCommercial(t);
	s.addCommercial(t1);
	s2.addCommercial(t);
	m.addSign(s);
	m.addSign(s2);

	CommercialPublisher c = new CommercialPublisher("commercialPub",
		"1234", app.getPublisherDB());
	c.register();
	c.setDisplay(d);
	shell = c.getShell();
	MediaCommercial media = new MediaCommercial(null, "", c, "test.png",
		shell);
	MediaCommercial media2 = new MediaCommercial(null, "", c, "swt.png",
		shell);
	media.simulateCommercial(0, 0);
	media2.simulateCommercial(120, 0);

	shell.open();
	// shell.close();
	while (!shell.isDisposed()) {
	    if (!d.readAndDispatch()) {
		d.sleep();
	    }
	}
	PublisherStatistics p = c.getPublisherStatistics();
	p.addCommercial(t);
	p.addDisplayTime(t, s, 10, 17);
	// so far no change - good
	s = new Sign(s, app.getSignDatabase());
	s.sendStatistics(t);
	// media.simulateCommercial();
	app.close();

    }

}
