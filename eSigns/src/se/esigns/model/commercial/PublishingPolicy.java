package se.esigns.model.commercial;



import java.util.Calendar;
import java.util.Date;

import se.esigns.model.publisher.Publisher;

//import org.w3c.dom.ranges.Range;

	public class PublishingPolicy {
			public enum PublishType { ONE_TIME, PERIODIC,PUSH;}
			
			
			PublishType type;
			Date startDate;
			Date EndDate;
			int fromHour;
			int tillHour;
			int everyHowManyDays;
			int datesDaysDelta;
		
			public PublishingPolicy(PublishType type,Date startDate,
					Date EndDate,Date fromHour,Date tillHour){
				
				this.type=type;
				this.startDate=startDate;
				this.EndDate=EndDate;
				Calendar c1=Calendar.getInstance();
				Calendar c2=Calendar.getInstance();
				c1.setTime(fromHour);
				this.fromHour=c1.get(Calendar.HOUR);
				c2.setTime(tillHour);
				this.tillHour=c2.get(Calendar.HOUR);
				datesDaysDelta=c2.get(Calendar.DAY_OF_YEAR)-c1.get(Calendar.DAY_OF_YEAR);
				this.everyHowManyDays=1;
			}
			public PublishingPolicy() {
			    // TODO Auto-generated constructor stub
			}
			public boolean timeToPublish(Date now)
			{
				Calendar c=Calendar.getInstance();
				c.setTime(now);
				Calendar startDate=Calendar.getInstance();
				startDate.setTime(this.startDate);
				int nowHour=c.get(Calendar.HOUR);
//				(Date now) is NOT between in the relevant days
				if ((now.compareTo(this.startDate)<0)||(now.compareTo(EndDate)>0)){
					return false;
				}
//				(Date now) is NOT between in the relevant Hours
				if (nowHour>this.tillHour || nowHour<this.fromHour)
				{
					return false;
				}
//				(Date now) is NOT satisfies 'thebeveryHowMany' Rule
				if (datesDaysDelta%everyHowManyDays>0)
				{
					return false;
				}
			return true;
			}

			public PublishType getType() {
				return type;
			}
			
	}
