package se.esigns.model.publisher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.MediaCommercial;
import se.esigns.model.commercial.PublishingPolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.sign.Sign;

@XmlSeeAlso({ OfficialPublisher.class })
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Publisher")
public class Publisher {

    private ArrayList<IPublisherDatabaseListener> listeners=new ArrayList<>();
    public HashMap<String, Sign> publishedInSigns = new HashMap<String, Sign>();

    private PublisherStatistics stats = new PublisherStatistics();
    private String username, password;

    /**
     * 
     * @param username
     * @param password
     * @param officialsData
     */

    public Publisher(String username, String password) {
	this.username = username;
	this.password = password;

    }

    public Publisher(Publisher p) {
	this.username = p.username;
	this.password = p.password;

    }

    public Publisher() {
	username = "Publisher";
	password = "";

    }

    public void registerPublisherDataListener(
	    IPublisherDatabaseListener listener) {
	listeners.add(listener);
    }

    public void unregisterPublisherDataListener(
	    IPublisherDatabaseListener listener) {
	listeners.remove(listener);

    }

    public <T> void accept(IPublisherVisitor<T> visitor) {
	visitor.visit(this);
    }

    public PublisherStatistics getPublisherStatistics() {
	return stats;
    }

    public void notifySignRemoved(Sign sign) {
	publishedInSigns.remove(sign);
    }

    public String getUsername() {
	return username;
    }

    public String getPassword() {
	return password;
    }

    @Override
    public boolean equals(Object otherPublisher) {
	Publisher oPub = (Publisher) otherPublisher;
	return (username.equals(oPub.getUsername())
		&& password.equals(oPub.getPassword()));
    }

    @Override
    public String toString() {
	return new StringBuffer(" Publisher Username : ").append(this.username)
		.append(" Publisher Password : ").append(this.password)
		.toString();
    }

    // commercial publisher

    /**
     * 
     * @param textCommercial
     */
    public TextCommercial createTextCommercial(String textCommercial,
	    PublishingPolicy p, Set<Sign> signs) {
	TextCommercial t = new TextCommercial(p, this, signs, textCommercial);
	return t;

    }

}