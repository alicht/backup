package se.esigns.model.management;

import se.esigns.model.publisher.OfficialPublisher;

public interface IManagerDatabase
	extends IMarketingDatabase, IPublishersDatabase, ISignsDatabase {
    public void registerManagerDataListener(IManagerDatabaseListener listener,
	    String username, String password);

    public boolean registerOfficial(String username, String password);

    public boolean promoteOfficial(String username);

    public boolean removePublisher(String username);

    public void demoteOfficial(String username);

}
