package se.esigns.controller.management;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import se.esigns.model.management.IManagerDatabase;
import se.esigns.model.management.IManagerDatabaseListener;
import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;
import se.esigns.model.sign.SignPricing;
import se.esigns.view.management.ManagerView;

public class ManagerController implements IManagerDatabaseListener {
	
	private IManagerDatabase data;
	private ManagerView view;
	private String managerUsername;
	private String managerPassword;
	/**
	 * 
	 * @param signsInchargeOf
	 * @param registerOfficial
	 */
	public ManagerController(IManagerDatabase managerData,ManagerView view) {
	    	//TODO: add manager registration
		this.data=managerData;
		this.view=view;
		this.data.registerManagerDataListener(this, managerUsername, managerPassword);
		this.view.registerController(this);

		
	}
	
	
	public void initUI(){
	    view.onInitUI();
	}
	/**
	 * 
	 * @param username
	 * @param password
	 */
	public OfficialPublisher addOfficial(String username, String password) {
		if (!data.registerOfficial(username, password))
		    return null;
		OfficialPublisher op=(OfficialPublisher) data.getPublisher(username);
		view.onPublisherAdded(op);
		return op;
	}
	
	public OfficialPublisher promoteToOfficial(String username) {
		if (!data.promoteOfficial(username))
		    return null;
		OfficialPublisher op=(OfficialPublisher) data.getPublisher(username);
		return op;
	}

	private PublishingPrivilege assignPrivilegeAll(){
		HashSet<AreaType> areaTypesPrivileged=new HashSet<AreaType>();
		areaTypesPrivileged.addAll(Arrays.asList(AreaType.values()));
		HashSet<String> areasPrivileged=new HashSet<String>();
		areasPrivileged.addAll(data.getAreas());
		PublishingPrivilege p = new PublishingPrivilege(areaTypesPrivileged,areasPrivileged);
		return p;
	}
	/**
	 * 
	 * @param OFFICIAL
	 */
	public void setPrivilege(OfficialPublisher publisher, PublishingPrivilege pp) {
		publisher.setPrivilege(pp);
		view.onPublisherPrivilegeEdited(pp, publisher);
	}

	/**
	 * 
	 * @param Sign
	 */
	public boolean removeSign(Sign sign) {
		if (!data.removeSign(sign)) return false;
//		
		return true;
	}

	/**
	 * 
	 * @param Sign
	 */
	public boolean addSign(Sign sign) {
		if (! data.addSign(sign)) return false;
//		
		return true;
	}

	public boolean addSign(String name, String location, AreaType areaType,boolean canImage) {
	    // TODO Auto-generated method stub
	    Sign s=new Sign(name,location,areaType,canImage);
	    return addSign(s);
	}


	public Set<Sign> getSigns() {
	    // TODO Auto-generated method stub
	    return data.getSigns();
	}


	


//	@Override
//	public void onSignEdited() {
//	    view.onSignEdited();
//	    
//	}
//
//
//	@Override
//	public void onPublisherPrivilegeEdited() {
//	    view.onPublisherPrivilegeEdited();
//	    
//	}


	@Override
	public void onPublisherAdded(Publisher publisher) {
	    view.onPublisherAdded(publisher);
	    
	}


	@Override
	public void onSignAdded(Sign sign) {

	    view.onSignAdded(sign);
	}


	@Override
	public void onPublisherPrivilegeEdited(PublishingPrivilege pp,
		Publisher publisher) {
	    // TODO Auto-generated method stub
	    
	}


	@Override
	public void onSignRemoved(Sign sign) {

	    view.onSignRemoved(sign);
	}


	@Override
	public void onPublisherRemoved(Publisher publisher) {
	    // TODO Auto-generated method stub
	    view.onPublisherRemoved(publisher);
	}


	public void removePublisher(String username2) {
	    // TODO Auto-generated method stub
	    data.removePublisher(username2);
	}


	public List<Publisher> getPublishers() {
	    // TODO Auto-generated method stub
	    return data.getAllPublishers();
	}


	@Override
	public void onSignEdited(Sign sign) {
	    // TODO Auto-generated method stub
	    
	}


	@Override
	public void onPublisherEdited(Publisher p) {
	    // TODO Auto-generated method stub
	    view.onPublisherEdited(p);
	}


	public void demoteOfficial(String username2) {
	    // TODO Auto-generated method stub
	    data.demoteOfficial(username2);
	}


	

}