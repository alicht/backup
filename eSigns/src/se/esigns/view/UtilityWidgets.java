package se.esigns.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class UtilityWidgets {

	public UtilityWidgets() {
    }


	public void MultySignPicker(Display display, TableItem[] item_list) {
	
		Device device = Display.getCurrent ();
	    final Color background_color = new Color(device, 200, 200, 220);
		
	    final Shell shell = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);
	    
	    RowLayout collum_layout = new RowLayout(SWT.VERTICAL);
	    shell.setLayout(collum_layout);
	    
	    shell.setBackground(background_color);
	    
	    //MenegerMainWorkPage widget definitions
	    
	    Label title_label = new Label(shell, SWT.CENTER);
	    Font header_lable_font = new Font(title_label.getDisplay(), new FontData("Arial", 12, SWT.BOLD));
	    title_label.setFont(header_lable_font);
	    title_label.setText("Sign Menager Site");
	    title_label.setBackground(background_color);
	    
	    final Table sign_table = new Table(shell, SWT.BORDER | SWT.MULTI);
        sign_table.setHeaderVisible(true);
        sign_table.setLinesVisible(true);
         
         String[] titles = { "Name", "Location", "Area Type", "Type"};
         
         for (int i = 0; i < titles.length; i++) {
             TableColumn column = new TableColumn(sign_table, SWT.NULL);
             column.setText(titles[i]);
             column.setWidth(130);
         }
         
         Composite button_row_composite = new Composite(shell,SWT.CENTER);
         RowLayout button_row_layout = new RowLayout(SWT.HORIZONTAL);
         button_row_composite.setLayout(button_row_layout);
	    
	    Button accept_button = new Button(button_row_composite, SWT.PUSH);
	    accept_button.setText("Ok");
	    
	    Button cancel_button = new Button(button_row_composite, SWT.PUSH);
	    cancel_button.setText("Cancel");
	          
	    //MenegerMainWorkPage Handle Listeners
	    
	    accept_button.addListener(SWT.Selection, new Listener() {
          	public void handleEvent(Event e) {
          		shell.dispose();
            }
          });
	    
	    cancel_button.addListener(SWT.Selection, new Listener() {
          	public void handleEvent(Event e) {
          		shell.dispose();
            }
          }); 
	    
	    
	    //MenegerMainWorkPage Formating
	     
	     
	     
	    
	    //loginUI run initialization
	    
	    shell.setText("Menager Site");
	    shell.pack();
	    shell.open();
	    
	    while (!shell.isDisposed()) {
	        if (!display.readAndDispatch())
	            display.sleep();
	    }        
	}
}