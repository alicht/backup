package se.esigns.model.sign;
import java.util.*;

import javax.swing.text.html.HTMLDocument.Iterator;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.TextCommercial;
/**
 * 
 * @author NIr
 *SIGN::
 *sign is the packge that manage the sign
 *to activate create instance of Sign : Sign x
 *and provoke x.run()
 *the sign will print it the relevant massges due to the 
 *commercials policy.
 *
 */
//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlRootElement(name="sign")
public class Sign {
//	Constants
	private int _SYNC_INTERVAL_SEC=120;
//	private int _STATISTICS_INTERVAL_SEC=240;
	private int _COMERCIAL_DURATION_SEC=10;
//	
//	STATE
	private final String name;
	private SignPricing pricing;
	private String areaName;
	SignStatistics stats=new SignStatistics();
	private AreaType areaType;
	
	private LinkedList<TextCommercial> push =new LinkedList<>();
	private Map<Commercial,Integer> commercialsMap=new HashMap<Commercial,Integer>();
	private boolean CanShowImage;
	private boolean SendStatistics;

	// BEHAVIOR
//	C'tors
	public Sign(String name2, SignPricing pricing2, String areaName2,
			AreaType areaType2,boolean canImage) {
	    this.name=name2;
		this.pricing=pricing2;
		this.areaName=areaName2;
		this.areaType=areaType2;
		this.CanShowImage=canImage;
		this.SendStatistics=false;
		
	}
	
	public Sign(Sign s,ISignDatabase db) {
		this(s.name,s.pricing,s.areaName,
				s.areaType,s.CanShowImage);
		this.SendStatistics=false;
		
	}
public Sign(String val1, String val2, AreaType valueOf,
		boolean boolean1) {
	    // TODO Auto-generated constructor stub
    		this(val1,new SignPricing(),val2,valueOf,boolean1);
	}

	//-----------------------------------------------
	public void setCommercials(Set<Commercial> commercials)
	{
	    this.commercialsMap.clear();
	    for (Commercial i : commercials)
	    {
		this.commercialsMap.put(i,0);
	    }
	}

	public void setPricing(SignPricing pricing) {
		this.pricing=pricing;
	}
	
	/**
	 * this function handle the activate the sign for a hole cycle
	 * 
	 */
	public void run_cycle(){
		int i=_COMERCIAL_DURATION_SEC;
		for (Map.Entry<Commercial,Integer> entry : commercialsMap.entrySet())
		{
			if (i==_SYNC_INTERVAL_SEC)
			{
				break;
			}
			if (push.size()>0)
			{
//				PRINT ON SCREEN
				i+=_COMERCIAL_DURATION_SEC;
				push.pop();
				continue;
			}
			entry.setValue(entry.getValue()+1);
			i+=_COMERCIAL_DURATION_SEC;
		}
		if (this.SendStatistics==true)
		{
			this.SendStatistics=false;
			sendStatistics();
			for (Map.Entry<Commercial,Integer> entry : commercialsMap.entrySet())
			{
				entry.setValue(0);
			}
		}
		else this.SendStatistics=true;
	}
	
	public void sendStatistics(){
//		TODO:
//		for every commercial c,R in statitics
//		updatetheDataBase  
//	    Commercial c=null;
//	    data.updatePublisherStatistics(c.getPublisher(), c, this, 10, 5);
	}
	
	public void pushToRuninglist(TextCommercial text){
		this.push.add(text);
	}

	public String getSignName() {
		return name;
		}
	
	public String getAreaName() {
		return areaName;
		}
	
	public AreaType getAreaType() {
		return areaType;
		}

	public SignPricing getPricing() {
		return pricing;
	}
	@Override
	public String toString() {     
		return new StringBuffer(" Sign Name : ")
		.append(this.name)
	    .append(" Area Name : ")
	    .append(this.areaName)
	    .append(" Area Type : ")
	    .append(this.areaType).toString();
	}
	@Override 
	public int hashCode() {
	   return (name+areaName+areaType).hashCode();
	}
	
	@Override
	public boolean equals(Object obj){
	    Sign o=(Sign)obj;
	    return (name.equals(o.name) && areaName.equals(o.areaName) && 
		    areaType.equals(o.areaType));
	}

	public boolean canIamge() {
	    // TODO Auto-generated method stub
	    return false;
	}
}