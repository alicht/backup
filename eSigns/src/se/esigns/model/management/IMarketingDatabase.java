package se.esigns.model.management;

import java.util.Set;

import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;

public interface IMarketingDatabase {
	public boolean editSign(Sign sign);

	public Set<Sign> getSigns();

	public Set<String> getAreas();
	
	public Set<Publisher> getRegisteredPublishers();
}
