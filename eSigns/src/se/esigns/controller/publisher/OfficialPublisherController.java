package se.esigns.controller.publisher;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlTransient;

import se.esigns.model.commercial.PublishingPolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.commercial.PublishingPolicy.PublishType;
import se.esigns.model.publisher.IOfficialDatabase;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;
import se.esigns.view.publisher.OfficialPublisherView;

public class OfficialPublisherController extends PublisherController {
    @XmlTransient
    public IOfficialDatabase officialsData;

    public OfficialPublisherController(IOfficialDatabase db,OfficialPublisherView view) {
	super(db,view);
	this.officialsData=db;
    }

    /**
     * 
     * @param TextCommercial
     */
    private void pushText(TextCommercial text, Set<Sign> signs) {
	for (Sign s : signs) {
	    officialsData.pushText(this, text, s);
	}
    }
    /**
	 * 
	 * @param textCommercial
	 */
	public TextCommercial createTextPush(String textCommercial,int minutesToShow, Set<Sign> signs) {
	    Calendar c1=Calendar.getInstance(),c2=Calendar.getInstance();
	    c2.add(Calendar.MINUTE, minutesToShow);
	    Date start=c1.getTime(),end=c2.getTime();
	    PublishingPolicy p=new PublishingPolicy(PublishType.PUSH,
	    		start,start,end,end);

	    ///NULL NULL NULL
		TextCommercial t=new TextCommercial(p, 
			officialsData.getPublisher(username,password),signs,textCommercial);
		return t;
		
	}
    
    /**
	 * 
	 * @param TextCommercial
	 */
	public void pushText(TextCommercial text,String area) {
		
		Set<Sign> signs=officialsData.getSigns(),toPublish=new HashSet<Sign>();
		for (Sign s : signs) if (s.getAreaName().equals(area)) 
			toPublish.add(s);
		pushText(text,toPublish);
		
	}
	/**
	 * 
	 * @param TextCommercial
	 */
	public void pushText(TextCommercial text,AreaType area) {
		
		Set<Sign> signs=officialsData.getSigns(),toPublish=new HashSet<Sign>();
		for (Sign s : signs) {
		    if (s.getAreaType().equals(area)) 
			toPublish.add(s);
		}
			
		pushText(text,toPublish);
	}
	
}