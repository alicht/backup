package se.esigns.view.management;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.widgets.Display;

import se.esigns.controller.management.ManagerController;
import se.esigns.model.management.IManagerDatabase;
import se.esigns.model.management.IManagerDatabaseListener;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.Sign;

public  class ManagerView implements IManagerDatabaseListener {
    private ManagerController managerController;
   // private IManagerDatabase managerData;
    private List<IManagerDatabaseListener> dataListeners=new ArrayList<>();
    public String username;
    public String password;
    ManagerMainWorkPage main ;
    Display display;
    public ManagerView( Display display){
//	this.managerData=data;
	this.display=display;
	
	dataListeners.add(main);
	
    }
    public void registerController(ManagerController controller){
	managerController=controller;
    }
    public ManagerController getController(){
	return managerController;
    }
   
    public void onInitUI() {
	main = new ManagerMainWorkPage(display,this);
	main.initUI();
	onRefreshSigns();
//	onRefreshPublishers();
    }
    
    public void onRefreshSigns() {
	// TODO Auto-generated method stub
	List<Sign> signs=new ArrayList<>();
	for (Sign s :managerController.getSigns())
	    signs.add(s);
	
    }
    
    public void onRefreshPublishers(){
	main.refreshPublishers(managerController.getPublishers());
    }
////    @Override
//    public void onSignEdited() {
//	for (IManagerDatabaseListener l : dataListeners);
//	   l.
//	
//    }
//    @Override
//    public void onPublisherPriviliges() {
//	// TODO Auto-generated method stub
//	
//    }
//    @Override
//    public void onPublisherPrivilegeEdited() {
//	// TODO Auto-generated method stub
//	
//    }
//    
//    @Override
//    public void onSignEdited() {
//	// TODO Auto-generated method stub
//	List<Sign> signs=new ArrayList<>();
//	signs.addAll(managerController.getSigns());
//	main.refreshSigns(signs);
//    }
//    @Override
//    public void onPublisherPrivilegeEdited() {
//	// TODO Auto-generated method stub
//	
//    }
    @Override
    public void onPublisherAdded(Publisher publisher){
	main.onPublisherAdded(publisher);
    }
    @Override
    public void onSignAdded(Sign sign) {
	// TODO Auto-generated method stub
	main.onSignAdded(sign);
    }
    @Override
    public void onPublisherPrivilegeEdited(PublishingPrivilege pp,
	    Publisher publisher) {
	// TODO Auto-generated method stub
	
    }
    @Override
    public void onSignRemoved(Sign sign) {
	// TODO Auto-generated method stub
	main.onSignRemoved(sign);
    }
    @Override
    public void onPublisherRemoved(Publisher publisher) {
	// TODO Auto-generated method stub
	main.onPublisherRemoved(publisher);
    }
    @Override
    public void onSignEdited(Sign sign) {
	// TODO Auto-generated method stub
	main.onSignEdited(sign);
    }
    @Override
    public void onPublisherEdited(Publisher p) {
	// TODO Auto-generated method stub
	main.onPublisherEdited(p);
    }
}
