package se.esigns.model.publisher;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.sign.Sign;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PublisherStatistics")
public class PublisherStatistics {
    
    public int accumulatedTime;
    int overallCharge;
    HashMap<Commercial, Stats> commercialsPublished=new HashMap<Commercial, Stats> ();

    public void addCommercial(Commercial commercial) {

	Stats s= new Stats();
	commercialsPublished.put(commercial, s);
    }

    public void addDisplayTime(Commercial commercial,Sign displayedIn, int secondsDisplayed,
	     int chargedAmount) {
	if (!commercialsPublished.containsKey(commercial)) {
	    return;
	}
	Stats s = commercialsPublished.get(commercial);
	if (s==null) return;
	s.displayTime += secondsDisplayed;
	s.charged += chargedAmount;
	Tuple t=new Tuple();
	t.signCharge=chargedAmount;
	t.signDisplayTime=secondsDisplayed;
	Tuple oldVal = s.publishedInSigns.putIfAbsent(displayedIn, t);
	if (oldVal!=null) {
	    oldVal.signCharge+=chargedAmount;
	    oldVal.signDisplayTime+=secondsDisplayed;
	    s.publishedInSigns.put(displayedIn, oldVal);
	}
	
	overallCharge+=chargedAmount;
	accumulatedTime+=secondsDisplayed;
    }
    
    

}