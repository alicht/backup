package se.esigns.model.sign;

public enum AreaType {
	Urban,
	Non_Urban;
}