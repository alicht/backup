package se.esigns.view.management;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.swing.text.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import se.esigns.ApplicationContext;
import se.esigns.controller.management.ManagerController;
import se.esigns.model.management.IManagerDatabaseListener;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PType;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;
import se.esigns.view.UtilityWidgets;

public class ManagerMainWorkPage implements IManagerDatabaseListener {

    private static int SIGN_COLLUM_NUMBER = 8;
    private static int PUBLISHER_COLLUM_NUMBER = 3;
 
    private ManagerController mc;
    private ManagerView managerView;

    /**
     * UI DATA
     * 
     * 
     * @param display
     * @param managerView
     */
    Device device;

    Color background_color;
    final Shell shell;
    RowLayout rowLayout = new RowLayout(SWT.VERTICAL);

    String[][] sign_list = {};

    Label title_label;
    Font header_lable_font;

    Composite sign_command_buttons;
    RowLayout Buttons_row = new RowLayout(SWT.HORIZONTAL);

    Button sign_editor_button;
    Button user_editor_button;
    final Composite work_area;

    Composite sign_editor;

    String[] titles;
    Table sign_table;
    Label table_name_label;
    Label location_edit_label;
    Label name_edit_label;
    Text name_edit_text;
    final Display display;

    Text location_edit_text;
    Label area_edit_label;

    Combo area_edit_combo;

    Label Type_edit_label;

    Combo type_edit_combo;

    Button add_button;
    Button edit_button;
    Button remove_button;

    public ManagerMainWorkPage(Display display, ManagerView managerView) {
	shell = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);
	work_area = new Composite(shell, SWT.CENTER);
	this.display = display;
	this.managerView = managerView;

	this.mc = managerView.getController();
    }

    public void initUI() {

	device = Display.getCurrent();
	background_color = new Color(device, 200, 200, 220);

	shell.setLayout(rowLayout);

	shell.setBackground(background_color);

	sign_list = new String[0][];

	// MenegerMainWorkPage widget definitions

	Label title_label = new Label(shell, SWT.CENTER);
	Font header_lable_font = new Font(title_label.getDisplay(),
		new FontData("Arial", 12, SWT.BOLD));
	title_label.setFont(header_lable_font);
	title_label.setText("Sign Menager Site");
	title_label.setBackground(background_color);

	sign_command_buttons = new Composite(shell, SWT.CENTER);
	sign_command_buttons.setLayout(Buttons_row);
	sign_command_buttons.setBackground(background_color);

	sign_editor_button = new Button(sign_command_buttons, SWT.PUSH);
	sign_editor_button.setText("Add or edit Signs");

	user_editor_button = new Button(sign_command_buttons, SWT.PUSH);
	user_editor_button.setText("Edit user privilages");

	work_area.setLayout(new FillLayout());
	work_area.setBackground(background_color);

	// MenegerMainWorkPage Handle Listeners

	sign_editor_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		Control[] children = work_area.getChildren();
		for (int i = 0; i < children.length; i++) {
		    children[i].dispose();
		}
		InitSignEditor(shell, work_area, background_color);
		work_area.layout();
		// shell.layout();
		shell.pack();
	    }
	});

	user_editor_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		Control[] children = work_area.getChildren();
		for (int i = 0; i < children.length; i++) {
		    children[i].dispose();
		}
		InitClearanceEditor(display, work_area, background_color);
		work_area.layout();
		// shell.layout();
		shell.pack();
	    }
	});

	// MenegerMainWorkPage Formating

	// loginUI run initialization

	shell.setText("Menager Site");
	shell.pack();
	shell.open();
	
	while (!shell.isDisposed()) {
	    if (!display.readAndDispatch())
		display.sleep();
	}
    }

    // ::::::::::::::::::::::::::
    // component functions
    // ::::::::::::::::::::::::::

    private void InitSignEditor(final Shell shell, Composite work_area,
	    Color background_color) {

	// InitSignEditor widget definitions

	sign_editor = new Composite(work_area, SWT.CENTER);
	sign_editor.setLayout(new GridLayout(SIGN_COLLUM_NUMBER, false));
	sign_editor.setBackground(background_color);

	table_name_label = new Label(sign_editor, SWT.CENTER);
	table_name_label.setText("Sign Table");
	table_name_label.setBackground(background_color);

	sign_table = new Table(sign_editor, SWT.BORDER);
	sign_table.setHeaderVisible(true);
	sign_table.setLinesVisible(true);

	String[] titles = { "Name", "Location", "Area Type", "Type" };

	for (int i = 0; i < titles.length; i++) {
	    TableColumn column = new TableColumn(sign_table, SWT.NULL);
	    column.setText(titles[i]);
	    column.setWidth(130);
	}

	name_edit_label = new Label(sign_editor, SWT.LEFT);
	name_edit_label.setText("Name:");
	name_edit_label.setBackground(background_color);

	name_edit_text = new Text(sign_editor, SWT.SINGLE);

	location_edit_label = new Label(sign_editor, SWT.LEFT);
	location_edit_label.setText("Location:");
	location_edit_label.setBackground(background_color);

	location_edit_text = new Text(sign_editor, SWT.SINGLE);

	area_edit_label = new Label(sign_editor, SWT.LEFT);
	area_edit_label.setText("area type:");
	area_edit_label.setBackground(background_color);

	area_edit_combo = new Combo(sign_editor, SWT.DROP_DOWN | SWT.READ_ONLY);
	for (AreaType areaType : AreaType.values())
	area_edit_combo.add(areaType.toString());


	Type_edit_label = new Label(sign_editor, SWT.LEFT);
	Type_edit_label.setText("Type:");

	type_edit_combo = new Combo(sign_editor, SWT.DROP_DOWN | SWT.READ_ONLY);
	type_edit_combo.add("Text only");
	type_edit_combo.add("Image or text");

	add_button = new Button(sign_editor, SWT.PUSH);
	add_button.setText("add");

	edit_button = new Button(sign_editor, SWT.PUSH);
	edit_button.setText("edit");

	remove_button = new Button(sign_editor, SWT.PUSH);
	remove_button.setText("remove");

	// InitSignEditor Handle Listeners

	sign_table.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		final TableItem[] selected_sign = sign_table.getSelection();
		name_edit_text.setText(selected_sign[0].getText(0));
		location_edit_text.setText(selected_sign[0].getText(1));
		area_edit_combo.select(
			area_edit_combo.indexOf(selected_sign[0].getText(2)));
		type_edit_combo.select(
			type_edit_combo.indexOf(selected_sign[0].getText(3)));
	    }
	});

	add_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		callAddSign(shell, sign_table, name_edit_text,
			location_edit_text, area_edit_combo, type_edit_combo);
	    }
	});

	edit_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		edditSign(shell, sign_table, name_edit_text, location_edit_text,
			area_edit_combo, type_edit_combo);
	    }
	});

	remove_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		removeSign(sign_table);
	    }
	});

	// InitSignEditor Formating

	GridData single_cell_data = new GridData(SWT.FILL, 0, true, true);
	GridData full_line_data = new GridData(SWT.FILL, 0, true, true);
	full_line_data.horizontalSpan = SIGN_COLLUM_NUMBER;
	GridData table_data = new GridData(SWT.FILL, 0, true, true);
	table_data.minimumHeight = 400;
	table_data.horizontalSpan = SIGN_COLLUM_NUMBER;

	table_name_label.setLayoutData(full_line_data);
	sign_table.setLayoutData(table_data);
	name_edit_label.setLayoutData(single_cell_data);
	name_edit_text.setLayoutData(single_cell_data);
	location_edit_label.setLayoutData(single_cell_data);
	location_edit_text.setLayoutData(single_cell_data);
	area_edit_label.setLayoutData(single_cell_data);
	Type_edit_label.setLayoutData(single_cell_data);
	type_edit_combo.setLayoutData(single_cell_data);
	add_button.setLayoutData(single_cell_data);
	edit_button.setLayoutData(single_cell_data);
	remove_button.setLayoutData(single_cell_data);

	
	managerView.onRefreshSigns();
    }
    Table publisher_table;
    private void InitClearanceEditor(final Display display, Composite work_area,
	    Color background_color) {

	// InitSignEditor widget definitions

	String[] publisher_list = { "CoccaCola", "mecdonalds", "Police",
		"Eroca" };

	Composite sign_editor = new Composite(work_area, SWT.CENTER);
	sign_editor.setLayout(new GridLayout(PUBLISHER_COLLUM_NUMBER, false));
	sign_editor.setBackground(background_color);

	Label table_name_label = new Label(sign_editor, SWT.CENTER);
	table_name_label.setText("Publisher List");
	table_name_label.setBackground(background_color);

	publisher_table = new Table(sign_editor, SWT.BORDER);
	publisher_table.setHeaderVisible(true);
	publisher_table.setLinesVisible(true);

	String[] titles = { "Publisher Name", "Official Publisher" };

	for (int i = 0; i < titles.length; i++) {
	    TableColumn column = new TableColumn(publisher_table, SWT.NULL);
	    column.setText(titles[i]);
	    column.setWidth(130);
	}

	for (int i = 0; i < publisher_list.length; i++) {
	    TableItem publisher = new TableItem(publisher_table, SWT.NULL);
	    publisher.setText(0, publisher_list[i]);
	    publisher.setText(1, "no");
	}

	Label privilage_edit_label = new Label(sign_editor, SWT.LEFT);
	privilage_edit_label.setText("Oficial Publisher");
	privilage_edit_label.setBackground(background_color);

	final Button privilage_edit_button = new Button(sign_editor, SWT.CHECK);
	privilage_edit_button.setBackground(background_color);
	privilage_edit_button.setEnabled(false);

	final Button edit_button = new Button(sign_editor, SWT.PUSH);
	edit_button.setText("edit");
	edit_button.setEnabled(false);
	
	final Button remove_button = new Button(sign_editor, SWT.PUSH);
	remove_button.setText("remove");
	remove_button.setEnabled(false);

	// InitSignEditor Handle Listeners

	publisher_table.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		final TableItem[] publisher = publisher_table.getSelection();
		privilage_edit_button.setEnabled(true);
		remove_button.setEnabled(true);
		if (publisher[0].getText(1) == "yes") {
		    privilage_edit_button.setSelection(true);
		    edit_button.setEnabled(true);
		    
		    
		} else {
		    privilage_edit_button.setSelection(false);
		    edit_button.setEnabled(false);

		    
		}
	    }
	});

	privilage_edit_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		final TableItem[] publisher = publisher_table.getSelection();
		String username=publisher[0].getText(0);
		if (privilage_edit_button.getSelection()) {
		    
		    mc.promoteToOfficial(username);
		    edit_button.setEnabled(true);
		} else {
		    mc.demoteOfficial(username);
		    edit_button.setEnabled(false);
		}
	    }
	});

	edit_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		edditPublisher(publisher_table, display);
	    }
	});
	
	remove_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		final TableItem[] publisher = publisher_table.getSelection();
		String username = publisher[0].getText();
		mc.removePublisher(username);
	    }
	});

	// InitSignEditor Formating

	GridData single_cell_data = new GridData(SWT.FILL, 0, true, true);
	GridData full_line_data = new GridData(SWT.FILL, 0, true, true);
	full_line_data.horizontalSpan = PUBLISHER_COLLUM_NUMBER;
	GridData table_data = new GridData(SWT.FILL, 0, true, true);
	table_data.minimumHeight = 400;
	table_data.horizontalSpan = PUBLISHER_COLLUM_NUMBER;

	table_name_label.setLayoutData(full_line_data);
	publisher_table.setLayoutData(table_data);
	privilage_edit_label.setLayoutData(single_cell_data);
	privilage_edit_button.setLayoutData(single_cell_data);
	edit_button.setLayoutData(single_cell_data);
	remove_button.setLayoutData(single_cell_data);
	
	managerView.onRefreshPublishers();
    }

    // ::::::::::::::::::::::::::
    // Utility functions
    // ::::::::::::::::::::::::::
    public void refreshPublishers(List<Publisher> publishers){
	publisher_table.removeAll();
	for (Publisher p : publishers){
	    addPublisher(p.getUsername(),
		    PType.Any.getType(p)==PType.OFFICIAL);
	}
    }
    private void addPublisher(String username, boolean official){
	TableItem publisher = new TableItem(publisher_table, SWT.NULL);
	    publisher.setText(0, username);
	    publisher.setText(1, official ? "Yes" : "No");
	
    }
    
//    private void removePublisher(String username, boolean official){
//   	TableItem publisher = new TableItem(publisher_table, SWT.NULL);
//   	    publisher.setText(0, username);
//   	    publisher.setText(1, official ? "Yes" : "No");
//   	
//       }
    
    public void refreshSigns(List<Sign> signs) {
	sign_table.removeAll();
	for (Sign s : signs) {
	    addSign(s.getSignName(), s.getAreaName(), s.getAreaType(), true);
	   
	}
    }

    public void addSign(String name, String location, AreaType areaType,
	    boolean canImage) {
	TableItem sign = new TableItem(sign_table, SWT.NULL);
	sign.setText(0, name);
	sign.setText(1, location);
	sign.setText(2, areaType.toString());
	sign.setText(3, String.valueOf(canImage));
	
	name_edit_text.setText("");
	location_edit_text.setText("");
	area_edit_combo.deselectAll();
	type_edit_combo.deselectAll();

    }

    private Sign getSelectedSign() {
	TableItem[] s = sign_table.getSelection();
	return new Sign(s[0].getText(0), s[0].getText(1),
		AreaType.valueOf(s[0].getText(2)),
		Boolean.getBoolean(s[0].getText(3)));
    }

    private void callAddSign(Shell shell, Table sign_table, Text name,
	    Text location, Combo area, Combo type) {
	String val1 = name.getText();
	String val2 = location.getText();
	String val3 = area.getText();
	String val4 = type.getText();
	if (val1.isEmpty() || val2.isEmpty() || val3.isEmpty()
		|| val4.isEmpty()) {
	    empty_field_error(shell, val1, val2, val3, val4);
	    return;
	}
	
	mc.addSign(new Sign(val1,val2,AreaType.valueOf(val3),
		Boolean.getBoolean(val4)));
	// c.addSign(name, val2, area, canImage);
	// TableItem sign = new TableItem(sign_table, SWT.NULL);
	// sign.setText(0,val1);
	// sign.setText(1,val2);
	// sign.setText(2,val3);
	// sign.setText(3,val4);
	//
	// TODO m.addSign(new Sign(name2, pricing2, areaName2, areaType2,
	// canImage, data))
	// m.addSign(val1,val2, val3, val4,true);

	System.out.println("  sign:"
		+ ApplicationContext.getApp().getPublisherDB().getSigns());

	
    }

    private void edditSign(Shell shell, Table sign_table, Text name,
	    Text location, Combo area, Combo type) {
	String val1 = name.getText();
	String val2 = location.getText();
	String val3 = area.getText();
	String val4 = type.getText();
	TableItem[] selected_sign = sign_table.getSelection();
	if (selected_sign.length == 0) {
	    unselected_error(shell, "Sign");
	    return;
	}
	if (val1.isEmpty() || val2.isEmpty() || val3.isEmpty()
		|| val4.isEmpty()) {
	    empty_field_error(shell, val1, val2, val3, val4);
	    return;
	}
	
	selected_sign[0].setText(0, val1);
	selected_sign[0].setText(1, val2);
	selected_sign[0].setText(2, val3);
	selected_sign[0].setText(3, val4);

    }

    private void removeSign(Table sign_table) {
	int selected_index = sign_table.getSelectionIndex();
	if (selected_index < 0) {
	    System.out.print("not selected");
	    return;
	}
	
	mc.removeSign(getSelectedSign());
	

    }

    private void edditPublisher(Table sign_table, Display display) {
	UtilityWidgets utils = new UtilityWidgets();
	TableItem[] item_list = null;
	utils.MultySignPicker(display, item_list);
	return;
    }

    void empty_field_error(Shell shell, String val1, String val2, String val3,
	    String val4) {
	String messege = "The following fields have not been filled:\n";
	if (val1.isEmpty()) {
	    messege += "\n\tName";
	}
	if (val2.isEmpty()) {
	    messege += "\n\tLocation";
	}
	if (val3.isEmpty()) {
	    messege += "\n\tArea";
	}
	if (val4.isEmpty()) {
	    messege += "\n\tType";
	}

	int style = SWT.ICON_ERROR | SWT.OK;

	MessageBox dia = new MessageBox(shell, style);
	dia.setText("Error");
	dia.setMessage(messege);
	dia.open();
    }

    void unselected_error(Shell shell, String object) {
	String messege = object + " not chosen";

	int style = SWT.ICON_ERROR | SWT.OK;

	MessageBox dia = new MessageBox(shell, style);
	dia.setText("Error");
	dia.setMessage(messege);
	dia.open();
    }

    @Override
    public void onPublisherAdded(Publisher publisher) {
	// TODO Auto-generated method stub
	addPublisher(publisher.getUsername(),
		PType.Any.getType(publisher)==PType.OFFICIAL);
    }

    @Override
    public void onSignAdded(Sign sign) {
	// TODO Auto-generated method stub
	addSign(sign.getSignName(),sign.getAreaName(),sign.getAreaType(),sign.canIamge());
    }

    @Override
    public void onPublisherPrivilegeEdited(PublishingPrivilege pp,
	    Publisher publisher) {
	// TODO Auto-generated method stub

    }

    private int getValueIndex(Table table, int column,String value){
	TableItem[] ti=table.getItems();
	int index=-1;
	for (TableItem t: ti){
	    String tableValue=t.getText(column);
	    if (tableValue.equals(value))
		index=table.indexOf(t);
	}
	return index;
    }
    
    @Override
    public void onSignRemoved(Sign sign) {
	// TODO Auto-generated method stub
	int index=getValueIndex(sign_table,0,sign.getSignName());
	if (index>-1) sign_table.remove(index);
	
    }

    @Override
    public void onPublisherRemoved(Publisher publisher) {
	// TODO Auto-generated method stub
	int index=getValueIndex(publisher_table,0,publisher.getUsername());
	if (index>-1) publisher_table.remove(index);
    }

    @Override
    public void onSignEdited(Sign sign) {
	// TODO Auto-generated method stub
	
    }

    @Override
    public void onPublisherEdited(Publisher p) {
	// TODO Auto-generated method stub
	int index=getValueIndex(publisher_table,0,p.getUsername());
	TableItem item=publisher_table.getItem(index);
	PType t=PType.Any.getType(p);
	item.setText(1,t==PType.OFFICIAL ? "Yes" : "No");
    }
}
