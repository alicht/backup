package se.esigns.controller.publisher;

import java.util.HashSet;
import java.util.Set;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.MediaCommercial;
import se.esigns.model.commercial.PublishingPolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.publisher.IPublisherDatabase;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;
import se.esigns.view.publisher.PublisherView;

public class PublisherController implements IPublisherDatabaseListener  {

    private IPublisherDatabase data;
    PublisherView view;
    String username;
    String password;

  
    public PublisherController(IPublisherDatabase data, PublisherView view) {

	this.data = data;
	
	this.view = view;
	view.registerController(this);
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public boolean isLoggedIn() {
	return data.isLoggedIn(this);
    }

    public boolean login() {
	if (data.isLoggedIn(this)) {
	    view.onLoginFail("User already logged in");
	    return false;
	}
	if (!data.login(this)){
	    view.onLoginFail("Invalid username or password");
	    return false;
	}
	    
	data.registerPublisherDataListener(this, username, password);
	view.onLogin();
	return true;
    }

    public boolean logout() {
	data.unregisterPublisherDataListener(this,username);
	view.onLogout();
	return data.logout(this);
	
    }

    public boolean register() {
	if (! data.registerPublisher(this)) {
	    view.onRegisterFail("Username taken");
	    return false;
	}
	view.onRegister();
	return true;
    }

//    /**
//     * 
//     * @param data
//     */
//    public void setSignsData(IPublisherDatabase data) {
//	this.data = data;
//    }

   
     
    public MediaCommercial createMediaCommercial(String image,
	    PublishingPolicy p, Set<Sign> signs) {
	// TODO:
	String id = "getIdFromDatabase";
	Publisher pub = data.getPublisher(username, password);
	MediaCommercial m = new MediaCommercial(p, id, pub, signs, image);
	return m;
    }

    /**
     * 
     * @param Commercial
     * @param area
     */
    public void publish(TextCommercial commercial, String area) {
	Set<Sign> signs = data.getSigns(), toPublish = new HashSet<Sign>();
	for (Sign s : signs)
	    if (s.getAreaName().equals(area))
		toPublish.add(s);
	publish(commercial, toPublish);
    }

    /**
     * 
     * @param Commercial
     * @param publishedInSigns
     */
    public void publish(Commercial commercial, Set<Sign> publishedInSigns) {
	for (Sign s : publishedInSigns)
	    data.publish(this, commercial, s);
    }

    public String getUsername() {
	return username;
    }

    public String getPassword() {
	return password;
    }

    public void initUI() {
	// TODO Auto-generated method stub
	view.initUI();
    }



    @Override
    public void onPublisherModelUpdate(Publisher publisher) {
	view.onPublisherModelUpdate(publisher);
	
    }


}
