package se.esigns.model.publisher;

import se.esigns.controller.publisher.OfficialPublisherController;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.sign.Sign;

public interface IOfficialDatabase extends IPublisherDatabase {
	public boolean pushText(OfficialPublisherController officialPublisherController,TextCommercial commercial,Sign sign);
	 public OfficialPublisher getOfficialPublisher(String username,String password);
}
