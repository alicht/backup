package se.esigns.model.management;

import java.util.List;

import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;

public interface IPublishersDatabase {
   

   

    public Publisher getPublisher(String username);
    
    public List<Publisher> getAllPublishers();

}