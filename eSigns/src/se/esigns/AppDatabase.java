package se.esigns;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import se.esigns.controller.management.ManagerController;
import se.esigns.controller.management.MarketerController;
import se.esigns.controller.publisher.OfficialPublisherController;
import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.PublishingPolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.management.IManagerDatabase;
import se.esigns.model.management.IManagerDatabaseListener;
import se.esigns.model.publisher.IOfficialDatabase;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.IPublisherVisitor;
import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PType;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.ISignDatabase;
import se.esigns.model.sign.Sign;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AppDatabase")
public class AppDatabase
	implements IOfficialDatabase, IManagerDatabase, ISignDatabase {

    private Set<ManagerController> managerControllers = new HashSet<ManagerController>();

    private Set<MarketerController> marketerControllers = new HashSet<MarketerController>();
    // From
    protected Map<String, Commercial> signCommericals = new HashMap<String, Commercial>();

    @XmlElement(name = "sign")
    protected Map<String, Sign> signs = new HashMap<String, Sign>();
    String[] areas = { "Krayot Imperia", "Winter is coming" };
    @XmlElement(name = "areanames")
    protected JaxbList<String> areaNames = new JaxbList<String>(
	    new ArrayList<String>(Arrays.asList(areas)));
    @XmlElement(name = "publisher")
    private Set<String> loggedIn = new HashSet<String>();
    private Map<String, Publisher> publishers = new HashMap<String, Publisher>();
    // private Set<Publisher> officials;
    private final String dbFileName;
    private ArrayList<IManagerDatabaseListener> managerListeners = new ArrayList<>();

    public AppDatabase() {
	dbFileName = "database_file";

    }

    public AppDatabase(String dbFileName) {

	this.dbFileName = dbFileName;
    }

    public void loadDatabase() throws JAXBException {
	File xmlFile = new File(dbFileName);
	if (!xmlFile.exists())
	    return;
	JAXBContext jaxbContext = JAXBContext.newInstance(AppDatabase.class,
		Publisher.class, Publisher.class, OfficialPublisher.class);
	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	AppDatabase db = (AppDatabase) jaxbUnmarshaller.unmarshal(xmlFile);
	this.signs = db.signs;
	// System.out.println(signs.toString());
	this.areaNames = db.areaNames;
	this.publishers = db.publishers;
	this.loggedIn = db.loggedIn;

	Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	jaxbMarshaller.marshal(this, System.out);

    }

    public void saveDatabase() throws JAXBException {
	File xmlfile = new File(dbFileName);
	JAXBContext jaxbContext = JAXBContext.newInstance(AppDatabase.class);
	Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	jaxbMarshaller.marshal(this, xmlfile);
	jaxbMarshaller.marshal(this, System.out);
    }

    public void addManager(ManagerController m) {
	managerControllers.add(m);
    }

    public void addMarketer(MarketerController m) {
	marketerControllers.add(m);
    }

    @Override
    public Set<Sign> getSigns() {
	HashSet<Sign> set = new HashSet<Sign>();
	for (Sign s : signs.values())
	    set.add(new Sign(s, null));
	return set;
    }

    @Override
    public Set<String> getAreas() {
	HashSet<String> set = new HashSet<String>();
	for (String s : areaNames.getList())
	    set.add(new String(s));
	return set;
    }

    // @Override
    // public boolean publish(Publisher publisher, Commercial commercial, Sign
    // sign) {
    // if (loggedIn.contains(publisher)) {
    // Sign s=signs.get(sign.toString());
    // s.addCommercial(commercial);
    // return true;
    // }
    // return false;
    // }

    @Override
    public boolean edit(PublisherController publisher, Commercial commercial, Sign sign) {
	if (loggedIn.contains(getIdentifier(publisher))) {
	    Sign s = signs.get(sign.toString());
	    // s.removeCommercial(commercial);
	    // s.addCommercial(commercial);
	    return true;
	}
	return false;
    }

    public boolean pushText(PublisherController publisher,
	    TextCommercial commercial, Sign sign) {
	if (loggedIn.contains(getIdentifier(publisher))
		&& publisher instanceof OfficialPublisherController) {
	    PublishingPrivilege privilege = ((OfficialPublisher)publishers
		    .get(publisher.getUsername())).getPrivilege();
	    if (privilege.allowed(sign)) {
		sign.pushToRuninglist(commercial);
		return true;
	    }
	}
	return false;
    }

    @Override
    public boolean addSign(Sign sign) {
	if (signs.containsValue(sign)) {
	    return false;
	}
	signs.put(sign.toString(), sign);
	for (IManagerDatabaseListener l : managerListeners)
	    l.onSignAdded(sign);
	return true;
    }

    @Override
    public boolean removeSign(Sign sign) {
	if (signs.remove(sign.toString()) == null)
	    return false;
	for (IManagerDatabaseListener l : managerListeners)
	    l.onSignRemoved(sign);

	return true;
    }

    @Override
    public boolean editSign(Sign sign) {
	signs.put(sign.toString(), sign);
	return true;
    }

    @Override
    public boolean registerOfficial(String username, String password) {

	if (publishers.containsKey(username)) {
	    // TODO: insert error "userName TAKEN"::::)))
	    return false;
	}

	PublishingPrivilege p = new PublishingPrivilege();
	OfficialPublisher o = new OfficialPublisher(username, password, this,
		p);
	publishers.put(username, o);
	return true;
    }

//    @Override
//    public boolean removeOfficial(String username) {
//	
//	return publishers.remove(username) != null;
//    }

    @Override
    public Set<Publisher> getRegisteredPublishers() {
	return new HashSet<Publisher>(publishers.values());
    }

    @Override
    public void updatePublisherStatistics(Publisher publisher,
	    Commercial commercial, Sign displayedIn, int secondsDisplayed,
	    int chargedAmount) {
	if (publishers.containsValue(publisher)) {
	    publishers.get(publisher.toString())
		    .getPublisherStatistics().addDisplayTime(commercial,
			    displayedIn, secondsDisplayed, chargedAmount);
	}
    }

    public boolean isRegistered(String username, String password) {
	return publishers.containsKey(username);
    }

    @Override
    public boolean isLoggedIn(PublisherController publisher) {
	return loggedIn.contains(getIdentifier(publisher));
    }
    
    private boolean validateUser(String username,String password){
	Publisher p = publishers.get(username);
	if (p==null || !p.getPassword().equals(password)) return false;
	return true;
    }
    
    public boolean isLoggedIn(String username,String password) {
	return loggedIn.contains(getIdentifier(username,password));
    }

    private String getIdentifier(String username,String password){
	return username+password;
    }
    
    private String getIdentifier(PublisherController publisher){
	return getIdentifier(publisher.getUsername(),publisher.getPassword());
    }
    
    @Override
    public boolean login(PublisherController publisher) {
	if (!publishers.containsKey(publisher.getUsername()) ||
		!validateUser(publisher.getUsername(), publisher.getPassword()))
	    return false;
	return loggedIn.add(getIdentifier(publisher));

    }

    @Override
    public boolean logout(PublisherController publisher) {
	if (!publishers.containsKey(publisher.getUsername()))
	    return false;
	return loggedIn.remove(getIdentifier(publisher));
    }

 

    @Override
    public boolean registerPublisher(PublisherController publisher) {
	// TODO Auto-generated method stub
	if (publishers.containsKey(publisher.getUsername())) return false;
	Publisher p = new Publisher(publisher.getUsername(),
		publisher.getPassword());

	publishers.put(p.getUsername(), p);
	p.accept(CopyPub.Any);
	Publisher newP=CopyPub.Any.copy;
//	loggedIn.add(getIdentifier(publisher));
	for (IManagerDatabaseListener l : managerListeners){
	    l.onPublisherAdded(newP);
	}
	return true;
    }

    @Override
    public boolean publish(PublisherController publisherController,
	    Commercial commercial, Sign sign) {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public Publisher getPublisher(String username, String password) {
	if (!validateUser(username, password))
	    return null;
	Publisher p=publishers.get(username);
	p.accept(CopyPub.Any);
	return  CopyPub.Any.copy;
    }

    @Override
    public boolean pushText(OfficialPublisherController publisher,
	    TextCommercial commercial, Sign sign) {
	Publisher p = publishers.get(publisher.getUsername());
	if (loggedIn.contains(publisher)
		&& PType.Any.visit(p)==PType.OFFICIAL) {
	    PublishingPrivilege privilege = ((OfficialPublisher)p).getPrivilege();
	    if (privilege.allowed(sign)) {
		sign.pushToRuninglist(commercial);
		return true;
	    }
	}
	return false;
    }

    @Override
    public OfficialPublisher getOfficialPublisher(String username, String password) {
	if (!validateUser(username,password)) return null;
	Publisher p = publishers.get(username);
	if (p==null || !(p instanceof OfficialPublisher))
	    return null;
	return new OfficialPublisher(p);
    }

    @Override
    public void registerManagerDataListener(IManagerDatabaseListener listener, String username, String password) {
	managerListeners.add(listener);
    }

    @Override
    public void registerPublisherDataListener(
	    IPublisherDatabaseListener listener, String username,
	    String password) {
	if (!validateUser(username,password) ) return;
	Publisher p=publishers.get(username);
	p.registerPublisherDataListener(listener);
	
    }

    @Override
    public boolean promoteOfficial(String username) {
	// TODO Auto-generated method stub
	Publisher p = publishers.get(username);
	if (p==null  || PType.Any.visit(p)== PType.OFFICIAL) return false;
	OfficialPublisher op=new OfficialPublisher(p);
	publishers.remove(username);
	publishers.put(username,op);
	for (IManagerDatabaseListener l : managerListeners){
	    l.onPublisherEdited(op);
	}
	return true;

    }

    @Override
    public boolean removePublisher(String username) {
	Publisher p= publishers.remove(username) ;
	if (p==null) return false;
	for (IManagerDatabaseListener l : managerListeners){
	    l.onPublisherRemoved(p);
	}
	return true;
    }
    
    private enum CopyPub implements IPublisherVisitor<Void> {
	Any;
	public  Publisher copy;
	@Override
	public Void visit(Publisher p) {
	    // TODO Auto-generated method stub
	    copy= new Publisher(p);
	    return null;
	}

	@Override
	public Void visit(OfficialPublisher p) {
	    // TODO Auto-generated method stub
	     copy=new OfficialPublisher(p);
	     return null;
	}
	
    }
    
    
//    private Publisher copyPub(Publisher p){
//	if (p==null) return null;
//	if (p instanceof OFFICIAL) return new OFFICIAL(p);
//	else return new Publisher(p);
//    }

    @Override
    public Publisher getPublisher(String username) {
	Publisher p= publishers.get(username);
	p.accept(CopyPub.Any);
	Publisher newP=CopyPub.Any.copy;
	return newP;
		
    }

    @Override
    public List<Publisher> getAllPublishers() {
	ArrayList<Publisher> list = new ArrayList<>();
	for (Publisher p : publishers.values()) {
	    p.accept(CopyPub.Any);
	    list.add(CopyPub.Any.copy);
	}
	
	return list;
    }

    @Override
    public void unregisterPublisherDataListener(
	    IPublisherDatabaseListener listener, String username) {
	// TODO Auto-generated method stub
	Publisher p=publishers.get(username);
	if (p==null) return;
	p.unregisterPublisherDataListener(listener);
	
    }

    @Override
    public void demoteOfficial(String username) {
	// TODO Auto-generated method stub
	Publisher p = publishers.get(username);
	if (p==null) return;
	publishers.remove(username);
	
	Publisher newP=new Publisher(p);
	publishers.put(username,newP);
	for (IManagerDatabaseListener l : managerListeners)
	    l.onPublisherEdited(new Publisher(newP));
    }

}
