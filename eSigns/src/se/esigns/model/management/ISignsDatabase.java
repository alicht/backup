package se.esigns.model.management;

import se.esigns.model.sign.Sign;

public interface ISignsDatabase {

    boolean addSign(Sign sign);

    boolean removeSign(Sign sign);

}