package se.esigns.controller.management;

import se.esigns.model.management.IMarketingDatabase;
import se.esigns.model.sign.Sign;
import se.esigns.model.sign.SignPricing;

public class MarketerController {

	
	IMarketingDatabase marketingData;

	/**
	 * 
	 * @param Sign
	 */
	public void notifySignRemoved(Sign sign) {
		marketingData.getRegisteredPublishers().forEach(pub->pub.notifySignRemoved(sign));
	}

	/**
	 * 
	 * @param Sign
	 * @param pricing
	 */
	public void setSignPrice(Sign sign, SignPricing pricing) {
		sign.setPricing(pricing);
		marketingData.editSign(sign);
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @param toNotify
	 * @param clientAppData
	 */
	public MarketerController(String username, String password, IMarketingDatabase marketingData) {
		this.marketingData=marketingData;
	}

}