package se.esigns.model.management;

import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.Sign;

public interface IManagerDatabaseListener {
    public void onSignAdded(Sign sign);
    public void onSignRemoved(Sign sign);
    public void onSignEdited(Sign sign);
    public void onPublisherPrivilegeEdited( PublishingPrivilege pp, Publisher publisher);
    public void onPublisherAdded(Publisher publisher);
    public void onPublisherRemoved(Publisher publisher);
    public void onPublisherEdited(Publisher p);
}
