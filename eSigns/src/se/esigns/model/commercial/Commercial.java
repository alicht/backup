package se.esigns.model.commercial;

import java.util.Date;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.swt.widgets.Shell;

import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;

//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlRootElement(name = "Commercial")
public abstract class Commercial {

	public PublishingPolicy policy;
	private   String id;
	private  Publisher publisher;
	private Set<Sign> SignesRequsted;
// C'tor	
	public Commercial(PublishingPolicy policy,String id,Publisher publisher, Set<Sign> SignsSet){
		this.policy=policy;
		this.id=id;
		this.publisher=publisher;
		this.SignesRequsted=SignsSet;
	}
	
	public boolean TimeToDisplay(Date now)
	{
		return this.policy.timeToPublish(now);
	}
	
	public Set<Sign> getCommercialsSigns()
	{
		return this.SignesRequsted;
	}

	public String getId() {
	    return id;
	}
	public Publisher getPublisher() {
	    return publisher;
	}
	public abstract void simulateCommercial(int x, int y) ;
	
	
	public PublishingPolicy getPublishingPolicy() { return policy; }
	

}