package se.esigns.model.publisher;

public interface IPublisherDatabaseListener {
    public void onPublisherModelUpdate(Publisher publisher);
}
