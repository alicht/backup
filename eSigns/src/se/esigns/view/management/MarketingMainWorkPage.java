package se.esigns.view.management;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class MarketingMainWorkPage {

	
	public MarketingMainWorkPage(Display display) {    
        initUI(display);
    }
    
    private void initUI(Display display) {
        
        Shell shell = new Shell(display, SWT.DIALOG_TRIM | SWT.CENTER);
        shell.setLayout(new FormLayout());
        
        
        Label text = new Label(shell, SWT.LEFT);
        text.setText("placeholder");
        FormData data = new FormData();
        data.top = new FormAttachment(0, 5);
        data.left = new FormAttachment(0, 5);
        text.setLayoutData(data);
        		
        shell.setText("placeholder");
        shell.pack();
        shell.open();
        
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }        
    }
}
